package com.gplusg.Services;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import com.gplusg.DS.Vector;

public class MessageService {
    public static String generateRandomMessage(){
        int len = (int)(Math.random() * 1e3 + 1);
        StringBuilder sb = new StringBuilder();
        while(len-- > 0){
            char c = (char)('a' + Math.random() * 26);
            sb.append(c);
        }
        return sb.toString();
    }
    public static Vector sha256(String input) throws NoSuchAlgorithmException {
        // Use SHA-256 algorithm
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] digest = md.digest(input.getBytes());
        // digest = Arrays.copyOfRange(digest, 0, );
        StringBuilder binaryString = new StringBuilder();

        for (byte b : digest) {
            for (int i = 7; i >= 0; i--) {
                binaryString.append((b >> i) & 1);
            }
        }
        Vector vec = new Vector(5);
        for(int i = 0; i < vec.getN(); i++)
            vec.set(i, (int)(binaryString.charAt(i) - '0'));
        return vec;
        
    }

}
