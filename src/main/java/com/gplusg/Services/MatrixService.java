package com.gplusg.Services;

import com.gplusg.DS.Matrix;
import com.gplusg.DS.Vector;

public class MatrixService {
    public Matrix add(Matrix mat1, Matrix mat2) {
        Matrix result = new Matrix(mat1.getN(), mat1.getM());
        for (int i = 0; i < mat1.getN(); i++) {
            for (int j = 0; j < mat1.getM(); j++) {
                result.set(i, j, mat1.get(i, j) + mat2.get(i, j));
            }
        }
        return result;
    }

    // Matrix subtraction
    public Matrix subtract(Matrix mat1, Matrix mat2) {
        Matrix result = new Matrix(mat1.getN(), mat1.getM());
        for (int i = 0; i < mat1.getN(); i++) {
            for (int j = 0; j < mat1.getM(); j++) {
                result.set(i, j, mat1.get(i, j) - mat2.get(i, j));
            }
        }
        return result;
    }
    
    public void scalarMultiply(Matrix mat, int x) {
        for (int i = 0; i < mat.getN(); i++) {
            for (int j = 0; j < mat.getM(); j++) {
                mat.set(i, j, mat.get(i, j) * x);
            }
        }
    }

    // Matrix modulo operation
    public void matMod(Matrix mat, int mod) {
        for (int i = 0; i < mat.getN(); i++) {
            for (int j = 0; j < mat.getM(); j++) {
                mat.set(i, j, ((mat.get(i, j) % mod) + mod) % mod);
            }
        }
    }
    public Matrix transpose(Matrix mat){
        int n = mat.getM(), m = mat.getN();
        Matrix ans = new Matrix(n, m);
        for(int i = 0; i < mat.getN(); i++){
            for(int j = 0; j < mat.getM(); j++)
                ans.set(j, i, mat.get(i, j));
        }
        return ans;
    }
    public Matrix JMat(int k, int m){
        Matrix mat = new Matrix(m, k);
        for(int i = 0; i < m; i++){
                mat.set(i, i, 1);
        }
        return transpose(mat);
    }
    public Vector matrixToVector(Matrix mat){
        int n = mat.getN();
        Vector vector = new Vector(n);
        for(int i = 0; i < n; i++)
            vector.set(i, mat.get(i, 0));
        return vector;
    }
    public Matrix multiply(Matrix mat1, Matrix mat2){
        Matrix mat = new Matrix(mat1.getN(), mat2.getM());
        for(int i = 0; i < mat.getN(); i++){
            for(int j = 0; j < mat.getM(); j++){
                for(int k = 0; k < mat2.getN(); k++){
                    mat.set(i, j, mat.get(i, j) + mat1.get(i, k) * mat2.get(k, j));
                }
            }
        }
        return mat;
    }
    public Matrix getScalarMultiply(Matrix mat, int x) {
        Matrix ans = new Matrix(mat.getN(), mat.getM());
        for (int i = 0; i < mat.getN(); i++) {
            for (int j = 0; j < mat.getM(); j++) {
                ans.set(i, j, mat.get(i, j) * x);
            }
        }
        return ans;
    }
}

