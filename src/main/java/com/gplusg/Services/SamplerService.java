package com.gplusg.Services;

import org.apache.commons.math3.distribution.MultivariateNormalDistribution;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import java.util.Random;
import com.gplusg.DS.Matrix;
import com.gplusg.DS.Vector;


public class SamplerService {
    private MatrixService matrixService = new MatrixService();
    private double sigma = 10;
    private double s = 0;
    public Matrix sampleMatrixFromZq(int n, int m, int q){
        Matrix mat = new Matrix(n, m);
        Random random = new Random();
        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                mat.set(i, j, random.nextInt(q));
            }
        }
        return mat;
    }
    public Vector sampleVectorFromZq(int n, int q){
        Vector vec = new Vector(n);
        Random random = new Random();
        for(int i = 0; i < n; i++){
            vec.set(i, random.nextInt(q));
        }
        return vec;
    }
    public Vector sampleVectorFromGaussianDistributionCovMat(int k, double covarianceMatrixData[][]){
        RealMatrix covarianceMatrix = MatrixUtils.createRealMatrix(covarianceMatrixData);
        return sampleFromZk(k, covarianceMatrix);
    }
    public Vector sampleFromZk(int k, RealMatrix covarianceMatrix) {
        MultivariateNormalDistribution distribution = new MultivariateNormalDistribution(new double[k], covarianceMatrix.getData());

        double[] sample = distribution.sample();
        Vector vec = new Vector(k);

        for (int i = 0; i < k; i++) {
            vec.set(i, (int) Math.round(sample[i]));
        }

        return vec;
    }
    public Vector sampleVectorFromGaussMatGiven(int K, Matrix mat){
        
        int k = mat.getN();
        Matrix iden = new Matrix(mat.getN());
        iden.transformToIdentityMatrix();
        double matrix[][] = new double[k][k];
        for(int i = 0; i < k; i++)
            matrix[i][i] = sigma * sigma;
        Matrix SSt = matrixService.multiply(mat, matrixService.transpose(mat));
        for(int i = 0; i < k; i++)
            for(int j = 0; j < k; j++)
                matrix[i][j] -= s * s * SSt.get(i, j);
        return sampleVectorFromGaussianDistributionCovMat(K, matrix);
    }
    public Vector sampleFromDiscreteGaussian(int l, double mean, double stdDev) {
        Vector sample = new Vector(l);
        Random random = new Random();

        for (int i = 0; i < l; i++) {
            double gaussianValue = generateGaussian(mean, stdDev, random);
            sample.set(i, (int) Math.round(gaussianValue));
        }

        return sample;
    }

    private double generateGaussian(double mean, double stdDev, Random random) {
        // Box-Muller transform to generate a standard Gaussian random number
        double u1 = 1.0 - random.nextDouble();
        double u2 = 1.0 - random.nextDouble();
        double z = Math.sqrt(-2.0 * Math.log(u1)) * Math.cos(2.0 * Math.PI * u2);

        // Adjust mean and standard deviation
        return mean + stdDev * z;
    }
    public Vector sampleKFromGaussian(int l, Vector v){
        long c = convertVectorToDecimal(v);
        c = -c / 2;
        return sampleFromDiscreteGaussian(l, s, c);
    }
    private long convertVectorToDecimal(Vector v){
        long ans = 0, pow = 1;
        for(int i = 0; i < v.getN(); i++){
            ans = ans + pow * v.get(i);
            pow <<= 1;
        }
        return ans;
    }
}
