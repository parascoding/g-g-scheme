package com.gplusg.Services;

import java.util.ArrayList;
import java.util.List;

import com.gplusg.DS.Matrix;

import com.gplusg.Entities.PublicKey;
import com.gplusg.Entities.PublicParameters;
import com.gplusg.Entities.SecretKey;

public class KeyGen {
    private MatrixService matrixService = new MatrixService();
    private SamplerService samplerService = new SamplerService();
    public void generateKey(PublicParameters publicParameters, PublicKey publicKey, SecretKey secretKey){
        // Gen A1
        Matrix A1 = samplerService.sampleMatrixFromZq(publicParameters.getM(), publicParameters.getK() - publicParameters.getM() - publicParameters.getL(), publicParameters.getQ());

        // Gen S1
        Matrix S1 = samplerService.sampleMatrixFromZq(publicParameters.getK() - publicParameters.getM() - publicParameters.getL(), publicParameters.getL(),publicParameters.getQ());

        // Gen S2
        Matrix S2 = samplerService.sampleMatrixFromZq(publicParameters.getM(), publicParameters.getL(),publicParameters.getQ());

        // Gen B
        Matrix B = matrixService.add(matrixService.multiply(A1, S1), S2);
        matrixService.matMod(B, publicParameters.getQ());


        // Gen qJ
        Matrix qJ = matrixService.JMat(publicParameters.getM(), publicParameters.getL());
        matrixService.scalarMultiply(qJ, publicParameters.getQ());


        // Gen A
        Matrix temp = new Matrix(publicParameters.getM(), publicParameters.getM());
        temp.transformToIdentityMatrix();
        List<Matrix> matList = new ArrayList<>();
        matList.add(matrixService.subtract(qJ, matrixService.getScalarMultiply(B, 2)));
        matList.add(matrixService.getScalarMultiply(A1, 2));
        matList.add(matrixService.getScalarMultiply(temp, 2));
        Matrix A = new Matrix(matList);

        // Gen S
        temp = new Matrix(publicParameters.getL());
        temp.transformToIdentityMatrix();
        matList = new ArrayList<>();
        matList.add(temp);
        matList.add(matrixService.transpose(S1));
        matList.add(matrixService.transpose(S2));
        Matrix S = matrixService.transpose(new Matrix(matList));
        
        publicKey.setPublicKey(A);
        secretKey.setSecretKey(S);


    }
}
