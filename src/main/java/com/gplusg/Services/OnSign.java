package com.gplusg.Services;

import com.gplusg.DS.Vector;
import com.gplusg.Entities.Commitment;
import com.gplusg.Entities.PublicKey;
import com.gplusg.Entities.PublicParameters;
import com.gplusg.Entities.SecretKey;
import com.gplusg.Entities.Signature;

public class OnSign {
    private VectorService vectorService = new VectorService();
    private SamplerService samplerService = new SamplerService();
    private MatrixService matrixService = new MatrixService();
    public void doOnlineSigning(Vector c, PublicParameters publicParameters, PublicKey publicKey, SecretKey secretKey, Commitment commitment, Signature signature){
        Vector k = samplerService.sampleKFromGaussian(publicParameters.getL(), c);
        vectorService.mod(k, 2 * publicParameters.getQ());
        Vector z = new Vector(publicParameters.getK());
        z = vectorService.add(z, commitment.getY());
        z = vectorService.add(z, 
            matrixService.matrixToVector(
                matrixService.getScalarMultiply(
                    matrixService.multiply(
                        secretKey.getSecretKey(), 
                        vectorService.vectorToMatrix(k)),
                2)
            )
        );
        z = vectorService.add(z, 
            matrixService.matrixToVector(
                matrixService.multiply(secretKey.getSecretKey(), vectorService.vectorToMatrix(c))
            )
        );
        vectorService.mod(z, 2 * publicParameters.getQ());
        signature.setSignature(z);
    }
}
