package com.gplusg.Services;

import com.gplusg.DS.Matrix;
import com.gplusg.DS.Vector;

public class VectorService {
    public Vector add(Vector a, Vector b){
        int n = a.getN(), m = b.getN();
        Vector ans = new Vector(Math.max(n, m));
        int i = 0, j = 0, k = 0;
        while (i < n && j < m) {
            ans.set(k++, a.get(i++) + b.get(j++));
        }
        while (i < n) {
            ans.set(k++, a.get(i++));
        }
        while (j < m) {
            ans.set(k++, b.get(j++));
        }
        return ans;
    }
    public void scalarMultiply(Vector vec, int k){
        for(int i = 0; i < vec.getN(); i++)
            vec.set(i, vec.get(i) * k);
    }
    public void mod(Vector vec, int k){
        for(int i = 0; i < vec.getN(); i++)
            vec.set(i, ((vec.get(i) % k) + k) % k);
    }
    public Matrix vectorToMatrix(Vector v){
        Matrix mat = new Matrix(v.getN(), 1);
        for(int i = 0; i < v.getN(); i++)
            mat.set(i, 0, v.get(i));
        return mat;
    }
}
