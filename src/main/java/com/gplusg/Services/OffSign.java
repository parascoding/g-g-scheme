package com.gplusg.Services;

import com.gplusg.Entities.Commitment;
import com.gplusg.Entities.PublicKey;
import com.gplusg.Entities.PublicParameters;
import com.gplusg.Entities.SecretKey;

public class OffSign {
    public void doOffLineSigning(PublicParameters publicParameters, PublicKey publicKey, Commitment commitment, SecretKey secretKey){
        commitment.setCommitment(publicParameters, publicKey, secretKey);
    }
}
