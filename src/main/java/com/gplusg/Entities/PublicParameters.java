package com.gplusg.Entities;

public class PublicParameters {
    private int m, k, l, q;
    public PublicParameters(int m, int k, int l, int q){
        this.m = m;
        this.k = k;
        this.l = l;
        this.q = q;
    }
    public int getM(){
        return m;
    }
    public int getK(){
        return k;
    }
    public int getQ(){
        return q;
    }
    public int getL(){
        return l;
    }
    public String toString(){
        StringBuilder sb = new StringBuilder("Bench\n");
        sb.append("M: " + m + "\n");
        sb.append("K: " + k + "\n");
        sb.append("L: " + l + "\n");
        sb.append("Q: " + q + "\n");
        return sb.toString();
    }
}
