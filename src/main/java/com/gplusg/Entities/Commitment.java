package com.gplusg.Entities;

import com.gplusg.DS.Matrix;
import com.gplusg.DS.Vector;
import com.gplusg.Services.MatrixService;
import com.gplusg.Services.SamplerService;
import com.gplusg.Services.VectorService;

public class Commitment {
    private Vector y;
    private Vector w;
    private SamplerService samplerService = new SamplerService();
    private MatrixService matrixService = new MatrixService();
    private VectorService vectorService = new VectorService();
    public Commitment(){

    }
    public void setCommitment(PublicParameters publicParameters, PublicKey publicKey, SecretKey secretKey){
        y = samplerService.sampleVectorFromGaussMatGiven(publicParameters.getK(), secretKey.getSecretKey());
        w = matrixService.matrixToVector(matrixService.multiply(publicKey.getPublicKey(), vectorService.vectorToMatrix(y)));
        // Matrix mat = matrixService.multiply(publicKey.getPublicKey(), vectorService.vectorToMatrix(y));
        // Matrix temp = vectorService.vectorToMatrix(y);
        // System.out.println("COMM:::");
        // System.out.println("Y:");
        // System.out.println(y);
        // System.out.println(temp);
        // System.out.println("A:");
        // System.out.println(publicKey.getPublicKey());
        // System.out.println("w = AY");
        // System.out.println(mat);
        // System.out.println("w ");
        // System.out.println(w);
        
        vectorService.mod(w, 2 * publicParameters.getQ());
    }
    public Vector getW(){
        return w;
    }
    public Vector getY(){
        return y;
    }
}
