package com.gplusg.Entities;

import com.gplusg.DS.Vector;

public class Signature {
    private Vector z;
    public Signature(){

    }
    public void setSignature(Vector z){
        this.z = z;
    }
    public Vector getSignature(){
        return this.z;
    }
    public String toString(){
        StringBuilder sb = new StringBuilder("Signature:\n");
        sb.append(z.toString());
        return sb.toString();
    }
}
