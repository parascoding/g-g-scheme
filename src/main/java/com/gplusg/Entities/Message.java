package com.gplusg.Entities;

import java.security.NoSuchAlgorithmException;

import com.gplusg.DS.Vector;
import com.gplusg.Services.MessageService;
import com.gplusg.Services.SamplerService;

public class Message {
    private String message;
    private Vector c;
    private SamplerService samplerService = new SamplerService();
    public Message(String message) throws NoSuchAlgorithmException{
        this.message = message;
        this.c = MessageService.sha256(message);
    }
    public Message(int l, int q){
        // System.out.println(l+" "+q);;
        this.c = samplerService.sampleVectorFromZq(l, 2);
    }
    public Vector getC(){
        return c;
    }
}
