package com.gplusg.Entities;

import com.gplusg.DS.Matrix;

public class PublicKey {
    private Matrix mat;
    public PublicKey(Matrix mat){
        this.mat = mat;
    }
    public Matrix getPublicKey(){
        return mat;
    }
    public void setPublicKey(Matrix mat){
        this.mat = mat;
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Public Key: \n");
        for(int i = 0; i < mat.getN(); i++){
            for(int j = 0; j < mat.getM(); j++){
                sb.append(mat.get(i, j) + " ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
