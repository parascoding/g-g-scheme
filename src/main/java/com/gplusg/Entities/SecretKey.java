package com.gplusg.Entities;

import com.gplusg.DS.Matrix;

public class SecretKey {
    private Matrix mat;
    public SecretKey(Matrix mat){
        this.mat = mat;
    }
    public Matrix getSecretKey(){
        return mat;
    }
    public void setSecretKey(Matrix mat){
        this.mat = mat;
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Secret Key: \n");
        for(int i = 0; i < mat.getN(); i++){
            for(int j = 0; j < mat.getM(); j++){
                sb.append(mat.get(i, j) + " ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
