package com.gplusg;

import java.io.FileWriter;
import java.io.PrintWriter;

import com.gplusg.DS.Matrix;
import com.gplusg.DS.Vector;
import com.gplusg.Entities.Commitment;
import com.gplusg.Entities.Message;
import com.gplusg.Entities.PublicKey;
import com.gplusg.Entities.PublicParameters;
import com.gplusg.Entities.SecretKey;
import com.gplusg.Entities.Signature;
import com.gplusg.Services.KeyGen;
import com.gplusg.Services.MatrixService;
import com.gplusg.Services.MessageService;
import com.gplusg.Services.OffSign;
import com.gplusg.Services.OnSign;
import com.gplusg.Services.VectorService;

public class Main{
    
    public static void main(String[] args) throws Exception{
        try {
            PrintWriter ot = new PrintWriter(new FileWriter("bench.txt"));
            System.out.println("Beginning G+G Scheme");
            publicParameters = new PublicParameters(256, 513, 256, 95233);
            
            double time = System.nanoTime();
            keyGen = new KeyGen();
            keyGen.generateKey(publicParameters, publicKey, secretKey);
            time = System.nanoTime() - time;
            ot.println("KeyGen");
            ot.println(publicParameters);
            ot.println("AVG TIME: " + time/1e3 + "us");
            int COUNT = 1;
            time = System.nanoTime();
            for(int i = 0; i < COUNT; i++){
                message = new Message(MessageService.generateRandomMessage());
                offSign.doOffLineSigning(publicParameters, publicKey, commitment, secretKey);
                onSign.doOnlineSigning(message.getC(), publicParameters, publicKey, secretKey, commitment, signature);
            }
            time = System.nanoTime() - time;
            time /= COUNT;
            time /= 1000;
            ot.println("\nSigning");    
            ot.println("AVG TIME: "+time+"us");
            // debug();
            ot.close();
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
    private static void debug(){
        System.out.println(publicKey);
        System.out.println(secretKey);
        System.out.println("Y:");
        System.out.println(commitment.getY());
        System.out.println("W:");
        System.out.println(commitment.getW());
        System.out.println("Challenge (C)");
        System.out.println(message.getC());
        System.out.println("Z:");
        System.out.println(signature.getSignature().subVector(publicParameters.getL()));
        Vector lhs = matrixService.matrixToVector(
            matrixService.multiply(publicKey.getPublicKey(), vectorService.vectorToMatrix(signature.getSignature()))
        );
        Matrix qJ = matrixService.JMat(publicParameters.getM(), publicParameters.getL());
        matrixService.scalarMultiply(qJ, publicParameters.getQ());
        Vector rhs = vectorService.add(
            commitment.getW(),
            matrixService.matrixToVector(
                matrixService.multiply(qJ, vectorService.vectorToMatrix(message.getC()))
            )
        );  
        
        vectorService.mod(lhs, 2 * publicParameters.getQ());
        vectorService.mod(rhs, 2 * publicParameters.getQ());
        System.out.println("LHS:");
        System.out.println(lhs);
        System.out.println("RHS:");
        System.out.println(rhs);
        // System.out.println("AS");
        // System.out.println(matrixService.multiply(publicKey.getPublicKey(), secretKey.getSecretKey()));
    }
    private static KeyGen keyGen;
    private static PublicParameters publicParameters;
    private static OffSign offSign = new OffSign();
    private static Signature signature = new Signature();
    private static Commitment commitment = new Commitment();
    private static OnSign onSign = new OnSign();
    private static Message message;
    private static MatrixService matrixService = new MatrixService();
    private static VectorService vectorService = new VectorService();
    private static PublicKey publicKey = new PublicKey(new Matrix(0));
    private static SecretKey secretKey = new SecretKey(new Matrix(0));
}