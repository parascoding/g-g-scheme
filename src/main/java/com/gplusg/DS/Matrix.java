package com.gplusg.DS;

import java.util.ArrayList;
import java.util.List;

public class Matrix {
    private List<List<Long>> list;
    private int n, m;
    public Matrix(int n, int m){
        this.n = n;
        this.m = m;
        list = new ArrayList<>();
        for(int i = 0; i < n; i++){
            List<Long> temp = new ArrayList<>();
            for(int j = 0; j < m; j++)
                temp.add(0l);
            list.add(temp);
        }
    }
    public Matrix(int n){
        this(n, n);
    }
    public Matrix(List<Matrix> matList){

        int n = matList.get(0).getN();
        int m = 0;
        for(int i = 0; i < matList.size(); i++)
            m += matList.get(i).getM();
        this.n = n;
        this.m = m;
        this.list = new ArrayList<>();
        for(int i = 0; i < n; i++){
            List<Long> temp = new ArrayList<>();
            for(int j = 0; j < m; j++)
                temp.add(0l);
            this.list.add(new ArrayList<>(temp));
        }
        
        for(int i = 0; i < n; i++){
            int ind = 0, j = 0, k = 0;
            while(k < m){
                list.get(i).set(k++, matList.get(ind).get(i, j++));
                if(j == matList.get(ind).getM()){
                    j = 0;
                    ind++;
                }
            }
        }
    }
    public void transformToIdentityMatrix(){
        for(int i = 0; i < n && i < m; i++)
            list.get(i).set(i, 1l);
    }
    public int getN(){
        return n;
    }
    public int getM(){
        return m;
    }
    public void set(int i, int j, long val){
        this.list.get(i).set(j, val);
    }
    public long get(int i, int j){
        return list.get(i).get(j);
    }
    public List<List<Long>> getMatrix(){
        return this.list;
    }
    public String toString(){
        StringBuilder sb = new StringBuilder("N = " + this.n +" M = "+this.m+"\n");
        for(int i = 0; i < n; i++){
            for(long x : list.get(i))
                sb.append(x+" ");
            sb.append("\n");
        }
        return sb.toString();
    }
}
