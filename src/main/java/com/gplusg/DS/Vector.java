package com.gplusg.DS;

import java.util.ArrayList;
import java.util.List;

public class Vector {
    private List<Long> list;
    private int n;
    public Vector(int n){
        this.n = n;
        list = new ArrayList<>();
        for(int i = 0; i < n; i++)
            list.add(0l);
    }
    public int getN(){
        return n;
    }
    public long get(int ind){
        return list.get(ind);
    }
    public List<Long> getVector(){
        return this.list;
    }
    public void set(int ind, long val){
        this.list.set(ind, val);
    }
    public String toString(){
        StringBuilder sb = new StringBuilder("DIM: "+n+"\n");
        for(long x : list)
            sb.append(x + " ");
        sb.append("\n");
        return sb.toString();
    }
    public Vector subVector(int len){
        Vector vec = new Vector(len);
        for(int i = 0; i < len; i++)
            vec.set(i, this.get(i));
        return vec;
    }
}
